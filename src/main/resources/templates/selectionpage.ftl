<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
<style>
body, html {
  height: 100%;
  margin: 0;
}
.background {
  background-image: url('mainpage.jpeg');

  height: 100%; 

  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* style the container */
.container {
  color:white;
   text-align: center;
  border-radius: 5px;
  
  padding: 20px 0 30px 0;
}

.btn {
   border-radius: 4px;
   background: linear-gradient(to right, #67b26b, #4ca2cb) !important;
   border: none;
   color: #FFFFFF;
   text-align: center;
   text-transform: uppercase;
   font-size: 22px;
   padding: 20px;
   width: 200px;
   transition: all 0.4s;
   cursor: pointer;
   margin: 5px;
 }
 .btn span {
   cursor: pointer;
   display: inline-block;
   position: relative;
   transition: 0.4s;
 }
 .btn span:after {
   content: '\00bb';
   position: absolute;
   opacity: 0;
   top: 0;
   right: -20px;
   transition: 0.5s;
 }
 .btn:hover span {
   padding-right: 25px;
 }
 .btn:hover span:after {
   opacity: 1;
   right: 0;
 }




</style>
        <title>login page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet"/>
        </head>
    <body>
    <div class="background">
         <div class="container">
            <h1>Real Estate Application</h1>
            <h2>Welcome ${loginPage.firstName}<h2>
            
                
                <form method="GET" action="/land">
                    <div class="button">
                       <button type="register" class="btn">Land Property </button>
                    </div>
                </form><br><br><br>
                <form method="GET" action="/home">
                    <div class="button">
                       <button type="register" class="btn">Home Property </button>
                    </div>
                </form><br><br><br>
                <form method="POST" action="/selectionpage">
                
                    <div class="button">
                       <button type="register" class="btn">Edit  Profile</button>
                    </div>
                </form>
                
            </div>
            </div>
        </body>
    </html>