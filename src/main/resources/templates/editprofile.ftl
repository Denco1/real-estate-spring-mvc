<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
<style>
</style>
        <title>edit customer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet"/>
        </head>
    <body>
         <div class="container">
            <h1>Real Estate Application</h1>
            <h2> Edit Profile And Change password</h2>
            <form method="POST" action="/editprofile" >
             
          
                <div>
                    <label for="firstName">First Name </label> 
                    <@spring.formInput "loginPage.firstName" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>

                <div class="form-group">
                    <label for="lastName">Last Name</label>
               
                    <@spring.formInput "loginPage.lastName" "class='form-control'" />
                     <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="address">Address</label>
                    <@spring.formInput "loginPage.address" "class='form-control'"/>
                     <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                
                
                <div class="form-group">
                    <label for="emailID">Email</label>
                    <@spring.formInput "loginPage.email" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="userName">Username</label>
                    <@spring.formInput "loginPage.userName" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="password">Password</label>
                    <@spring.formPasswordInput "loginPage.password" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>

                <div class="form-">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
                
                </form>
                
            </div>
        </body>
    </html>