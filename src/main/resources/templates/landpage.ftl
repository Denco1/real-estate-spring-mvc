<!DOCTYPE html>
<html>
<head>
<style>
* {
  box-sizing: border-box;
}

.column {
  float: left;
  width: 33.33%;
  padding: 5px;
}

/* Clearfix (clear floats) */
.row::after {
  content: "";
  clear: both;
  display: table;
}
.container {
  color:Blue;
   text-align: center;
  border-radius: 5px;
  
  padding: 20px 0 30px 0;
} 
</style>
</head>
<body>
<div class="container">
<h1>Welcome to the Land property page</h1>
<h2>Select your land property here</h2>
</div>
<form method="POST" action="/land">

<div class="row">
  <div class="column">
    <img src="LandA.jpg" height="300" style="width:100%"><br>
    <button class="button" style="width:100%">Buy Now</button>
  </div>
  <div class="column">
    <img src="landB.jpeg"  height="300" style="width:100%"><br>
    <button class="button" style="width:100%">Buy Now</button>
  </div>
  <div class="column">
    <img src="LandC.jpg"  height="300" style="width:100%"><br>
    <button class="button" style="width:100%">Buy Now</button>
  </div>
</div>
</form>

</body>
</html>