<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
<style>
</style>
        <title>new customers</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet"/>
        </head>
    <body>
         <div class="container">
            <h1>Real Estate Application</h1>
                <form method="POST" action="/newcustomer">
                

                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <@spring.formInput "customer.firstName" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>

                <div class="form-group">
                    <label for="lastName">Last Name</label>
               
                    <@spring.formInput "customer.lastName" "class='form-control'" />
                     <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="address">Address</label>
                    <@spring.formInput "customer.address" "class='form-control'"/>
                     <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                
                
                <div class="form-group">
                    <label for="emailID">Email</label>
                    <@spring.formInput "customer.email" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="userName">Username</label>
                    <@spring.formInput "customer.userName" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>
                
                <div class="form-group">
                    <label for="password">Password</label>
                    <@spring.formPasswordInput "customer.password" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>

                <div class="form-">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                
                
                </form>
            </div>
        </body>
    </html>