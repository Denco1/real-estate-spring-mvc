<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html>
<head>
<style>
body, html {
  height: 100%;
  margin: 0;
}
.background {
  background-image: url('main.jpg');

  height: 100%; 

  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* style the container */
.container {
  color:white;
   text-align: center;
  border-radius: 5px;
  
  padding: 20px 0 30px 0;
} 


/* style inputs and link buttons */
input,
.btn {
  width: 25%;
  padding: 12px;
  border: round;
  border-radius: 4px;
  margin: 5px 0;
  opacity: 0.85;
  display: inline-block;
  font-size: 17px;
  line-height: 20px;
  text-decoration: none; /* remove underline from anchors */
}
.form-group{
	color:white;
   text-align: left;
  border-radius: 5px;
  
  

}


</style>
        <title>login page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel="stylesheet"/>
        </head>
    <body>
    <div class="background">
         <div class="container">
         
            <h1>Real Estate Application</h1>
            <h2>Login Page</h2>
                
                

                                <form method="POST" action="/">
                                

                <div class="form-group">
                    <label for="userName">Username</label>
                    <@spring.formInput "loginPage.userName" "class='form-control'"/>
                    
                </div>
                
                
                

                <div class="form-group">
                    <label for="password">Password</label>
                    <@spring.formPasswordInput "loginPage.password" "class='form-control'"/>
                    <div><@spring.showErrors "<br>" "bg-danger"/></div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
                
                </form>
                <form method="GET" action="/newcustomer">
                    <div class="form-group">
                    
                       <button type="register" class="btn btn-primary">Sign up</button>
                    </div>
                </form>
                
                </div>
                
                
            </div>
        </body>
    </html>