package de.hsm.realestate.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import de.hsm.realestate.customerManagment.CustomerManagment;
import de.hsm.realestate.persistent.Customer;

@Controller
@Scope("session")
public class RegistartionController {
	
	@GetMapping("/")
	public ModelAndView login(LoginPage loginPage) {
		ModelAndView modelView= new ModelAndView();
		modelView.setViewName("login");
		return modelView;
	}
	
	@PostMapping("/")
	public ModelAndView showLogin(LoginPage loginPage, HttpSession session, BindingResult bindingResult, Customer customer)
	{
		
		ModelAndView modelView = new ModelAndView();
		CustomerManagment customerManager= new CustomerManagment();
		
		if(customerManager.loginCustomer(loginPage.getUserName(), loginPage.getPassword())== null)
		{
			modelView.setViewName("login");
			modelView.addObject(loginPage);
            String [ ] codes = {"Password.Wrong"};
            FieldError fe = new FieldError("user", "password",
                                                loginPage.getPassword(), false, codes, null, "This is default error");
            bindingResult.addError(fe);
            return modelView;
		} 
		
		loginPage= (LoginPage) session.getAttribute("customer");
		
		modelView.setViewName("selectionpage");
		//loginPage.setFirstName(customer.getFirstName());
		modelView.addObject(loginPage);
		customerManager.loginCustomer(loginPage.getUserName(), loginPage.getPassword());
		session.setAttribute("login", loginPage);
		
		return modelView;
		
		
	}
	 
	
	
	@GetMapping("/newcustomer")
	public ModelAndView newCustomer(Customer customer,BindingResult bindingResult)
	{
		ModelAndView modelView = new ModelAndView();
		modelView.setViewName("newcustomer");
		return modelView;
		
	}
	@PostMapping("/newcustomer")
	public ModelAndView showNewCusotmer(@Valid Customer customer, BindingResult bindingResult, HttpSession session,RedirectAttributes redirectAttributess) {
		
		ModelAndView modelView = new ModelAndView();
		CustomerManagment customerManager= new CustomerManagment();
		
		if(!customerManager.checkEmail(customer.getEmail()))
		{
			String [ ] codes = {"Email.In.use"};
            FieldError fe = new FieldError("customer", "email",
                                                customer.getEmail(), false, codes, null, "This is default error");
            bindingResult.addError(fe);
            modelView.setViewName("newcustomer");
            return modelView;
		}
		
		if(bindingResult.hasErrors())
		{
			 modelView.setViewName("newcustomer");
	            return modelView;
		}
		 modelView.setViewName("login");
		  LoginPage loginPage= new LoginPage();
		   loginPage.setFirstName(customer.getFirstName());
		  loginPage.setEmail(customer.getEmail());
		  
		  loginPage.setLastName(customer.getLastName());
		  loginPage.setUserName(customer.getUserName());
		  loginPage.setPassword(customer.getPassword());
		  loginPage.setAddress(customer.getAddress());
		  
		  modelView.addObject(loginPage);
		  //modelView.addObject("firstName", loginPage);
		 // modelView.addObject(firstName);
		  
		  
		  session.setAttribute("customer", loginPage);
		   
		  customerManager.registerCustomer(customer);
		return modelView;
		
	}

}
