package de.hsm.realestate.controller;

public class Payment {
 
 private String bankName;
 private String creditCard;
 

public String getBankName() {
	return bankName;
}
public void setBankName(String bankName) {
	this.bankName = bankName;
}
public String getCreditCard() {
	return creditCard;
}
public void setCreditCard(String creditCard) {
	this.creditCard = creditCard;
}
 
 
}
