package de.hsm.realestate.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import de.hsm.realestate.customerManagment.CustomerManagment;
import de.hsm.realestate.persistent.Customer;

@Controller
@Scope("session")
public class SelectionController {
	

	
	@GetMapping("/selectionpage")
	public ModelAndView selectionPage(LoginPage loginPage) {
		ModelAndView modelView= new ModelAndView();
		
		modelView.setViewName("selectionpage");
		return modelView;
	}
	@PostMapping("/selectionpage")
	public ModelAndView showNewCusotmer(LoginPage loginPage, BindingResult bindingResult , HttpSession session) {
		
		ModelAndView modelView = new ModelAndView();
		//CustomerManagment customerManager= new CustomerManagment();
		
		 loginPage=(@Valid LoginPage) session.getAttribute("customer");
		 
		
		
		
		if(bindingResult.hasErrors())
		{
			 modelView.setViewName("selectionpage");
	            return modelView;
		}
		 modelView.setViewName("editprofile");

		  modelView.addObject(loginPage);
		  
		  session.setAttribute("customer", loginPage);
		  
		  
		return modelView;
		
	}
	
	@GetMapping("/land")
	public ModelAndView landPage(LoginPage loginPage)
	{
		ModelAndView modelView= new ModelAndView();
		
		modelView.setViewName("landpage");
		return modelView;
		
	}
	@PostMapping("/land")
	public ModelAndView showLandPage(LoginPage loginPage, BindingResult bindingResult , HttpSession session) {
		ModelAndView modelView= new ModelAndView();
		
		loginPage= (LoginPage) session.getAttribute("customer");
		//System.out.println("inside paymnet"+loginPage.getFirstName());
		
		
		modelView.setViewName("payment");
		/*
		 * PaymentPage paymentPage=new PaymentPage();
		 * paymentPage.setFirstName(loginPage.getFirstName());
		 * paymentPage.setLastName(loginPage.getLastName());
		 * System.out.println("value from paymnet"+paymentPage.getFirstName());
		 * 
		 */		
		modelView.addObject(loginPage);
		
		return modelView;
	}
	
	 
	@GetMapping("/payment")
	public ModelAndView paymentPage(Payment payment) {
		ModelAndView modelView= new ModelAndView();
		
		
		 
		modelView.setViewName("payment");
		return modelView;
		
		
	}
	@PostMapping("/payment")
	public ModelAndView showPaymentPage(LoginPage loginPage, HttpSession session, Payment payment) {
		ModelAndView modelView= new ModelAndView();
		loginPage=(LoginPage) session.getAttribute("customer");
		modelView.addObject(loginPage);
		modelView.setViewName("payment");
        return modelView;
		
		
	}
	
	@GetMapping("/home")
	public ModelAndView homePage(LoginPage loginPage)
	{
		ModelAndView modelView= new ModelAndView();
		
		modelView.setViewName("homepage");
		return modelView;
		
	}
	@PostMapping("/home")
	public ModelAndView showHomePage(LoginPage loginPage, BindingResult bindingResult , HttpSession session) {
		ModelAndView modelView= new ModelAndView();
		
		loginPage= (LoginPage) session.getAttribute("customer");
		//System.out.println("inside paymnet"+loginPage.getFirstName());
		
		
		modelView.setViewName("payment");
		/*
		 * PaymentPage paymentPage=new PaymentPage();
		 * paymentPage.setFirstName(loginPage.getFirstName());
		 * paymentPage.setLastName(loginPage.getLastName());
		 * System.out.println("value from paymnet"+paymentPage.getFirstName());
		 * 
		 */		
		modelView.addObject(loginPage);
		
		return modelView;
	}
	
	@GetMapping("/editprofile")
	public ModelAndView editProfile( LoginPage loginPage)
	{
		ModelAndView modelView= new ModelAndView();
		modelView.setViewName("editprofile");
		
		
		return modelView;
		
	}
	@PostMapping("/editprofile")
	public ModelAndView ShowEditProfile(LoginPage loginPage, HttpSession session ,@Valid Customer customer, BindingResult bindingResult)
	{
		ModelAndView modelView= new ModelAndView();
		CustomerManagment customerManager= new CustomerManagment();
		
		loginPage= (LoginPage) session.getAttribute("customer");
		 if(bindingResult.hasErrors())
		 {
			 modelView.setViewName("editprofile");
			 return modelView;
		 }
		
		loginPage.setFirstName(customer.getFirstName());
		loginPage.setEmail(customer.getEmail());
		  
		  loginPage.setLastName(customer.getLastName());
		  loginPage.setUserName(customer.getUserName());
		  loginPage.setPassword(customer.getPassword());
		  loginPage.setAddress(customer.getAddress());
		
		modelView.setViewName("selectionpage");
		
		modelView.addObject(loginPage);
		session.setAttribute("customer", loginPage);
		customerManager.registerCustomer(customer);
		
		
		return modelView;
		
	}
	@GetMapping("/confirmation")
	public ModelAndView confirmationpage(LoginPage loginPage)
	{
		ModelAndView modelView= new ModelAndView();
		modelView.setViewName("confirmation");
		
		
		return modelView;
		
	}
	@PostMapping("/confirmation")
	public ModelAndView showConfirmationpage(LoginPage loginPage, HttpSession session)
	{
		ModelAndView modelView= new ModelAndView();
		modelView.setViewName("selectionpage");
		loginPage= (LoginPage) session.getAttribute("customer");
		modelView.addObject(loginPage);
		return modelView;
		
	}
	

}
