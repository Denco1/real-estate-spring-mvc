package de.hsm.realestate.customerManagment;

import java.util.List;

import javax.validation.Valid;

import de.hsm.realestate.persistent.Customer;
import de.hsm.realestate.persistent.Persistent;
import de.hsm.realestate.persistent.QueryName;

public class CustomerManagment {
	
	private final Persistent persistent ;
	
	public CustomerManagment() {
		
		persistent= Persistent.getInstance();
		
	}
	
	public void registerCustomer(@Valid Customer customer) {
		persistent.addCustomer(customer);
		
	}
	public List<Customer> searchCustomer(String username)
	{
		return persistent.executeQuery(Customer.class, QueryName.searchName,username);
	}
	public Customer loginCustomer(String userName, String password) {
		return persistent.find(Customer.class, userName, password);
		
	}
	
	
	
	public Boolean checkEmail(String email)
	{
		return persistent.findEmail(email);
	}

	

}
