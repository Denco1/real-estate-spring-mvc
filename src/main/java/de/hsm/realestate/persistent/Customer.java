package de.hsm.realestate.persistent;

import javax.validation.constraints.NotEmpty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Customer {
	
	@NotEmpty(message="Firts name cannot be empty")
	private String firstName;
	
	@NotEmpty
	private String lastName;
	
	
	@NotEmpty
	private String address;
	
	@NotEmpty
    private String userName;
	
	@NotEmpty
	private String email;
	
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")
    @NotEmpty
    @Size(min = 5, max = 100)
	private String password;
	
	
	
	public Customer(String firstName, String lastName, String address, String userName, String email, String password) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName= userName;
		this.address = address;
		this.email = email;
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	

}
