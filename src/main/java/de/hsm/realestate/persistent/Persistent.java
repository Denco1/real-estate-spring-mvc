package de.hsm.realestate.persistent;

import java.util.ArrayList;
import java.util.List;

public class Persistent {
	
	private List<Customer> customers;
	
	private static Persistent instance = new Persistent();
	public Persistent() {
		
		customers = new ArrayList<Customer>();
	
	}
	public void addCustomer(Customer customer) {
		customers.add(customer);
    }
	
	
    public <T> List<T> executeQuery(Class<T> type, QueryName queryName, Object arg) {
        List<T> rst = new ArrayList<>();
        if (type.equals(Customer.class) && queryName == QueryName.searchName) {
            String name = (String) arg;
            for (Customer Customer : customers) {
                if (Customer.getUserName().equals(name)) {
                    rst.add((T) Customer);
                }
            }
        }
        return rst;
    }
    


    public <T> T find(Class<T> type, Object key1, Object key2) {
        if (type.equals(Customer.class)) {
            String username = (String) key1;
            String password = (String) key2;
            for (Customer customer : customers) {
                if (customer.getUserName().equals(username) && customer.getPassword().equals(password)) {
                    return (T) customer;
                }
            }
        }
        return null;
    }
   
 public Boolean findEmail(String email)
 {
	 Boolean rs=true;
	 for(Customer customer : customers)
	 {
		 if(customer.getEmail().equals(email))
		 {
			 return false;
		 }
	 }
	 return rs;
 }
  
 public static Persistent getInstance() {
     if (instance == null) {
         instance = new Persistent();
     }
     return instance;
 }
 public void persist(Object o) {
     
 }

 
}
